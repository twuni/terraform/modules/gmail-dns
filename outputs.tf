output "spf" {
  description = "SPF record for Gmail. Delegates to Google."
  value       = "v=spf1 include:_spf.google.com -all"
}

output "mx" {
  description = "MX records for Gmail."
  value       = [
    "10 aspmx.l.google.com.",
    "20 alt1.aspmx.l.google.com.",
    "20 alt2.aspmx.l.google.com.",
    "30 aspmx2.googlemail.com.",
    "30 aspmx3.googlemail.com.",
    "30 aspmx4.googlemail.com.",
    "30 aspmx5.googlemail.com.",
  ]
}
