# Gmail DNS | Terraform Modules | Twuni

This module provides a set of static values for DNS records for configuring Gmail on a domain.

## Requirements

No requirements.

## Inputs

No inputs.

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_mx"></a> [mx](#output\_mx) | MX records for Gmail. |
| <a name="output_spf"></a> [spf](#output\_spf) | SPF record for Gmail. Delegates to Google. |
